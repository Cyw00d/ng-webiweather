import { fuckoff } from './../../content/foaas';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-domains',
  templateUrl: './domains.component.html',
  styleUrls: ['./domains.component.css']
})
export class DomainsComponent implements OnInit {

  public apiResponse;
  public domain;
  public tld;
  public error;
  public fuckOff = fuckoff;
  public selectedLine;
  public inputsArray: string[] = [];

  public formObjects: Object = {};

  constructor(private _api: ApiService) { }

  ngOnInit() {
  }

  checkDomain() {
    this.error = null;
    this.apiResponse = null;
  }

  dropdownUpdate() {
    this.getSelectedLine();
  }

  doFfApiCallDan() {
    this._api.doFuckOffCall(this.selectedLine.split('/')[1], this.formObjects, this.inputsArray).subscribe(data => {
      this.apiResponse = data;
    },
      e => this.error = e
    );
  }

  getSelectedLine() {
    if (!this.selectedLine) { return; }
    const regex = new RegExp(/\:[a-zA-Z]\w+/, 'g');
    this.inputsArray = this.selectedLine.match(regex);
  }

}
