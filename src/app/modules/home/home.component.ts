import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public buienradar: Object;

  constructor(private _api: ApiService) { }

  ngOnInit() {
    this._api.getBuienradar().subscribe(data => {
      this.buienradar = data;
    }, error => {
      window.console.error('Buienradar api error:', error);
    });
  }

}
