import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  // private httpOptions = {
  //   headers: new HttpHeaders({ 'Content-Type': 'application/json; charset=UTF-8' }),
  // };

  constructor(private http: HttpClient) { }

  getBuienradar(): Observable<object> {
    return this.http.get('https://api.buienradar.nl/data/public/1.1/jsonfeed');
  }

  // getBuienradarr(): Observable<any> {
  //   return this.http.get<any>(`https://api.buienradar.nl/data/public/1.1/jsonfeed`);
  // }

  doFuckOffCall(dir, attributes, inputsList) {
    let attr = '';

    if (inputsList.length > 0) {
      inputsList.forEach(a => {
        attr = attr ? attr + '/' + attributes[a] : attributes[a];
      });
    }

    return this.http.get<any>(`https://www.foaas.com/${dir}/${attr}`);
  }
}
